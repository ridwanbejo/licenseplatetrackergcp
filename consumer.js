// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);
const Datastore = require('@google-cloud/datastore');

// Your Google Cloud Platform project ID
const projectId = 'serverlessid';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

// Creates a client
const datastore = new Datastore({
  projectId: projectId,
});

const subscriptionName = 'licensePlateTrackerSubscription';
const timeout = 60;

// References an existing subscription
const subscription = pubsubClient.subscription(subscriptionName);
const uuidv4 = require('uuid/v4');

/**
 * Triggered from a message on a Cloud Pub/Sub topic.
 *
 * @param {!Object} event The Cloud Functions event.
 * @param {!Function} The callback function.
 */
exports.storeLicensePlateTrackerData = (event, callback) => {
    // The Cloud Pub/Sub Message object.
    const pubsubMessage = event.data;

    // The kind for the new entity
    const kind = 'licensePlates';

    // The name/ID for the new entity
    const name = uuidv4();
    // The Cloud Datastore key for the new entity
    const licensePlateDataKey = datastore.key([kind, name]);

    // Prepares the new entity
    const licensePlateData = {
      key: licensePlateDataKey,
      data: JSON.parse(Buffer.from(pubsubMessage.data, 'base64').toString()),
    };

    console.log(licensePlateData);

    // Saves the entity
    datastore
      .save(licensePlateData)
      .then(() => {
        console.log(`Saved ${licensePlateData.key.name}`);
      })
      .catch(err => {
        console.error('ERROR:', err);
      });
  
    // Don't forget to call the callback.
    callback();
};
