// Imports the Google Cloud client library
const Storage = require('@google-cloud/storage');

const projectId = 'serverlessid';

// Creates a client
const storage = new Storage({
  projectId: projectId,
});

/**
 * TODO(developer): Uncomment the following lines before running the sample.
 */
const bucketName = 'staging.serverlessid.appspot.com';
const filename = '../data/sample1.jpg';
const destination = "licensePlate/sample6-1.jpg";

// Uploads a local file to the bucket
let bucket = storage.bucket(bucketName);

bucket.upload(filename, {destination: destination})
  .then(() => {
    console.log(`${filename} uploaded to ${bucketName}.`);    

    bucket
	  .file(destination)
	  .makePublic()
	  .then(() => {
	    console.log(`gs://${bucketName}/${destination} is now public.`);
	  })
	  .catch(err => {
	    console.error('ERROR:', err);
	  });
  })
  .catch(err => {
    console.error('ERROR:', err);
  });

