
// Imports the Google Cloud client library
const vision = require('@google-cloud/vision');

// Creates a client
const client = new vision.ImageAnnotatorClient();

// client
//   .labelDetection('https://www.googleapis.com/download/storage/v1/b/staging.serverlessid.appspot.com/o/licensePlate%2Fsample.jpg?generation=1520858044266983&alt=media')
//   .then(results => {
//     const labels = results[0].labelAnnotations;

//     console.log('Labels:');
//     labels.forEach(label => console.log(label.description));
//   })
//   .catch(err => {
//     console.error('ERROR:', err);
//   });
 
client
  .textDetection('https://www.googleapis.com/download/storage/v1/b/staging.serverlessid.appspot.com/o/licensePlate%2Fsample.jpg?generation=1520858044266983&alt=media')
  .then(results => {
    const detections = results[0].textAnnotations;
    console.log('Text:');
    console.log(detections);
    console.log(detections[0].description);
    detections.forEach(text => console.log(text));
  })
  .catch(err => {
    console.error('ERROR:', err);
  });