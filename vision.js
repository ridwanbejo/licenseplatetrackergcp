
// Imports the Google Cloud client library
const vision = require('@google-cloud/vision');
const PubSub = require('@google-cloud/pubsub');
// const Storage = require('@google-cloud/storage');

// Creates a client
const projectId = "serverlessid";

// Creates a client
const client = new vision.ImageAnnotatorClient({
	projectId: projectId,
});

const pubsubClient = new PubSub({
  projectId: projectId,
});

/*
const storage = new Storage({
  projectId: projectId,
});
*/

const topicName = "licensePlateTracker";
// const bucketName = 'staging.serverlessid.appspot.com';

// let bucket = storage.bucket(bucketName);

exports.consumerLicensePlateTracker = (event, callback) => {

	console.log('Processing file: ' + event.data.name);
	// console.log(event.data.mediaLink);
	client
		.textDetection(event.data.mediaLink)
		.then(results => {
			const detections = results[0].textAnnotations;
			
			var payload = {
				licensePlate: detections[0].description,
				ocrRawData: detections,
				createdAt: new Date(),
				gcpFileName: event.data.name,
				gcpMediaLink: event.data.mediaLink
			};
		
			// console.log(payload);
	  		
	  		console.log('VISION API EXECUTION IS SUCCESS!');
	  		
	  		const dataBuffer = Buffer.from(JSON.stringify(payload));
		    // console.log(dataBuffer);
	  
			pubsubClient
				.topic(topicName)
				.publisher()
				.publish(dataBuffer)
				.then(results => {
				  const messageId = results[0];
				  console.log(`Message ${messageId} published.`);
				})
				.catch(err => {
				  console.error('ERROR:', err);
				});
		})
		.catch(err => {
			console.error('VISION API ERROR:', err);
		});

	 /*	
	 bucket
		  .file(event.data.name)
		  .makePublic()
		  .then(res => {
		    // console.log(`gs://${bucketName}/${event.data.name} is now public.`);
			
		    
		  })
		  .catch(err => {
		    console.error('BUCKET ERROR:', err);
		  });

	*/

	callback();
};
