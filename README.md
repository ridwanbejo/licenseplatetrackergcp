#License Plate Tracker

Basic Feature:

- Upload image from camera to Cloud Storage via HTTP
- Scan the license plate using Cloud Vision API
- Store the log if license plate is pass through the parking area

Current Schema:

```
upload to cloud storage from localhost -> cloud function use cloud vision api -> send cloud vision api result to cloud pubsub  -> receive by cloud function then store to cloud datastore				
```

Later Schema:

```
upload image to cloud function -> receive by cloud storage from cloud function -> cloud function use cloud vision api -> send cloud vision api result to cloud pubsub  -> receive by cloud function then store to cloud datastore					
```
