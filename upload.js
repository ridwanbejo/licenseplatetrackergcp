// Imports the Google Cloud client library
const Storage = require('@google-cloud/storage');

const projectId = 'serverlessid';

// Creates a client
const storage = new Storage({
  projectId: projectId,
});

/**
 * TODO(developer): Uncomment the following lines before running the sample.
 */
const bucketName = 'staging.serverlessid.appspot.com';
const filename = './data/sample.jpg';

export licencePlateUploader = function(event, callback){
	// Uploads a local file to the bucket
	let bucket = storage.bucket(bucketName);

	// receive image with busyboy NPM package

	// upload image to cloud storage
	bucket.upload(filename, {destination: "licensePlate/sample1.jpg"})
	  .then(() => {
	    console.log(`${filename} uploaded to ${bucketName}.`);
	  })
	  .catch(err => {
	    console.error('ERROR:', err);
	  });

};